![JKubeOrg](images/JKubeOrg.png)

# JKube Linker #

This tool allows linking a google cloud project to a bitbucket account in order to start using the [JKube](JKube.md) framework. 

## What is JKube? ##

JKube is an [open source](https://en.wikipedia.org/wiki/Open_source) initiative for creating a quick and hassle free solution to allow 
[software developers/engineers](https://www.temok.com/blog/software-developer-software-engineer/) building, deploying and 
running their application code with [Kubernetes](https://en.wikipedia.org/wiki/Kubernetes). 

## What's the idea? ##

In order to use JKube, you just need to [link your git account to a cloud provider](HOWTO.md). 
JKube will automatically set up the required [repositories](https://en.wikipedia.org/wiki/Software_repository) in the background for you. 
Starting your first [Kubernetes cluster](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-architecture) is then just one git push away. 

All necessary [customization](JKube.md) can be done simply by editing text files in the repository. 
No need to get introduced into the magic of [kubectl ymls](https://kubernetes.io/docs/reference/kubectl/overview/) (unless you enjoy it).

## Getting started ##

### Prerequisites ###

Currently only one combination of Git provider/Cloud hoster is supported:
[Atlassian BitBucket](https://bitbucket.org/product/) <-> [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine).

Before you can start, you need: 

* a [Google cloud](https://cloud.google.com) project ([Create here](https://console.cloud.google.com/freetrial/signup/tos))
* a [BitBucket](https://bitbucket.org/product/) account (([Sign up here](https://bitbucket.org/account/signup/))

### Steps to link accounts ###

1. Login to [gcloud console](https://console.cloud.google.com/)  and select target project
2. Open cloud shell console (little blue '>' icon on the upper right)
3. Execute `docker run -it jkube/link` and follow [instructions](HOWTO.md).

## Do I have to build the docker image? ##

There is no need to build the docker image yourself. The latest version is already hosted at [DockerHub](https://hub.docker.com/repository/docker/jkube/link).

However, if for some reason you need/want to create the docker image locally, you can do that with two simple operations:

```
mvn package
docker build -t jkube/link .
```

