
# How To use the JKube Linker #

## Step 1: Activate Google Compute Engine  ##

If you have not used Google's compute services before, the first thing you have to do is to activate GCE in your Google account. 
If you do not have a Google account, sign up [here](https://accounts.google.com/signup). 

At the moment of this writing (January 2021) Google offers a 3 month free trial including a start credit of $300. To activate 
go to [https://cloud.google.com/](https://cloud.google.com/). The page should look something like this (probably with other content and in another language):

![JKubeOrg](images/gce1.png)

---

Make sure to select (1) the google account that you would like to use (in case you have multiple), then click on the blue button (2) 
to start the free trial.

The assistant will make you fill in several details; you will also have to provide some payment information (credit card).
Don't worry, you will not be billed after your start credit has been used up, just your machines will stop working. 
Google will not take any money before you actively enable payments (by pressing a button somewhere). 
 
## Step 2: Create a new project  ##

You can have multiple independent projects in the GCE. If you just enabled compute engine, you can use the automatically created 
project (called "My first project" or similar, you may change the name if you like). 

If you already have some other GCE projects, we recommend to create a new one for JKube. To do so, you have to click on the currently 
active project in the header bar:

![JKubeOrg](images/newproject1.png)

Next click on "New Project" in the upper right corner of the dialog window:

![JKubeOrg](images/newproject2.png)

You can then enter an appropriate name for your new JKube project...

![JKubeOrg](images/gce2.png)

## Step 3: Activate Kubernetes  ##

Before starting the linking procedure, you have to activate Kubernetes for your JKube project. 
To do so select the project and then click on the menu (three line in the header bar left),

![JKubeOrg](images/activate.png)

then scroll down and click on "Kubernetes Engine":

![JKubeOrg](images/activate2.png)

The activation process will be started automatically and you will see the following message:

![JKubeOrg](images/gce4.png)

You have wait until the activation process is finished (the rotating circle disappears). 

NOTE: After the activation has finished, you will be given the option to create a Kubernetes cluster.
You should not do that, because JKube activates and deactivates Kubernetes clusters for you.

![JKubeOrg](images/gce5.png)

## Step 4: Start the linking procedure ##

Select your JKube project and click on the little ">_" icon on the right side of the header bar to open a cloud shell:

![JKubeOrg](images/gce6.png)

In the cloud shell enter `docker run -it jkube/link` and press enter.

![JKubeOrg](images/gce7.png)

After downloading the docker image you will be asked to provide the project number, which you can find under project information.

![JKubeOrg](images/projectid.png)
	
Cut and paste the project number (not the project ID) into the cloud shell window and press enter.

You will (the first time at least) be asked to authenticate the cloud shell by the following dialog:

![JKubeOrg](images/auth.png)


## Step 5: Creating BitBucket App Password ##

Next, the JKube linker will ask you to provide your bitbucket credentials. To find them log in to [bitbucket](https://bitbucket.org)
and click on your avatar initials on the bottom of the side bar left:

![JKubeOrg](images/bb1.png)

Click on "personal settings" 

![JKubeOrg](images/bb2.png)

and you can find your bitbucket user id in the window that pops up under "Username":

![JKubeOrg](images/bb3.png)

Type in your username into the cloud shell window and press enter. 

Next you will be asked for a password. Do NOT enter your normal Bitbucket user password here! 
Instead create an "app password" for which you can fine tune permissions and which you can always revoke should it become 
necessary.
 
![JKubeOrg](images/bb4.png)

After you clicked "Create app password" you must specify a label (you can choose any name you like) and select the following
permissions:

* Account: Read
* Workspace membership: Read
* Projects: Write
* Repositories: Write & Admin
* Pipelines: Edit variables

![JKubeOrg](images/bb5.png)

After clicking "Create" a window with the app passord appears which you can copy and paste into the cloud shell:

![JKubeOrg](images/bb6.png)

## Step 6: Create clusters repository and start your first Kubernetes cluster ##

When the JKube linker has confirmed that it has access to your bitbucket account it will display all your workspaces 
and lets you select the one that you would like to use (just enter the corresponding number).

Finally you can specify the name of the project (bitbucket allows grouping of related repositories into "projects", a 
bitbucket workspace may contain several projects). The linker tool proposes a name (constructed out of the GCE project name).
If the name is okay, simply press enter.

The linker tool will then create a project and populate it with a repository that is used control your Kubernetes 
clusters. In case of success, the linking process will display the following message:

![JKubeOrg](images/success.png)

The message in the cloud shell window tells you the URL of this repository. You can clone out the repository with your favorite git tool or IDE (or you could directly edit it online with bitbucket's 
integrated web-editor). The repository holds a file "clusters.md" with a table that is pre-filled with an example 
Hello-world-cluster specification. Change the entry "DELETED" to "RUNNING" and commit and push. This will automatically 
trigger a process that sets up and starts your cluster (thanks to the pipeline specification and repository level variables 
which the linker has already installed for you). 

After a couple of minutes you will receive an email indicating that a configuration change is being processed. Some time later
(typically 10-15 minutes) you will receive another mail that informs you that the new Kubernetes cluster has been created. 
Finally, again a couple of minutes later you will receive a third and final mail, informing you that your cluster has been started 
and initialized. In that mail you will find (amongst other information) 
the url under which you can access your "Hello-World" application on the web.

